# SpringSchool24R-MoTMo



## About this model 

This repository provides the model code of the Reduced Mobility Transition Model (R-MoTMo), version 1.1.0, for the ASU MATH+ spring school on agent-based modeling 2024. For a documentation see [its entry in the CoMSES library](https://www.comses.net/codebases/eef9e270-909b-4832-8228-c2f3a839f171/releases/1.0.0/), the only changes from that model version are in the naming of some functions. A working paper on this model can be found [here](https://hdl.handle.net/21.11116/0000-000E-3C3C-D).
